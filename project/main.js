// 1) DOM это объектная модель документа.Этот метод говорит нам о том, что с помощью него мы можем видоизменять наши элементы с помощью JS

// 2) innerText показывает все текста, которые у нас есть в HTML, а innerHTML показывает текст только по одному элементу

// 3) Самый распространненый способ обратиться к элементу через querySelector, querySelectorAll, getElementById, а способы getElementsByTagName или getElementsByClassName и т.д устаревшие



let paragraph = document.querySelectorAll('p'); // 1)
for (elem of paragraph) {
    elem.style.background = '#ff0000';
}


let idName = document.getElementById('optionsList'); // 2)
let parent = idName.parentNode;
let children = idName.childNodes;
console.log(idName);
console.log(parent);
console.log(children);


let id = document.getElementById('testParagraph'); // 3)
let change = id.innerHTML = "This is a paragraph";


let mainHeader = document.querySelectorAll('.main-header')[0]; // 4)
for (let value of mainHeader.children) {
    value.classList.add("nav-item");
    console.log(value);
}

let tag = document.querySelectorAll('.section-title'); // 5)
for (let value of tag) {
    console.log(value);
    value.classList.remove('section-title');
}